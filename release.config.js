const releaseRules = [
  // This custom rule ensures that documentation updates can be released without
  // having to wait for fixes or new features
  //
  // Default release rules do not include any release for documentation changes
  // @see https://github.com/semantic-release/commit-analyzer/blob/master/lib/default-release-rules.js
  { type: 'doc', release: 'patch' },
  { type: 'docs', release: 'patch' }
]

/**
 * These options ensure that the updated changelog is committed to the project
 */
const gitOptions = {
  assets: ['CHANGELOG.md', 'dist/**/*', 'package.json']
}

module.exports = {
  plugins: [
    // Analyzes commit to determine fixes, features and breaking changes
    ['@semantic-release/commit-analyzer', { releaseRules }],
    // Creates release notes from the changes found prior
    '@semantic-release/release-notes-generator',
    // Updates release notes in a conventional CHANGELOG format
    '@semantic-release/changelog',
    // Updates `package.json` version
    '@semantic-release/npm',
    // Creates a new version tag commit, updates `CHANGELOG.md` and `package.md`
    ['@semantic-release/git', gitOptions]
  ]
}
