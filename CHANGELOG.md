## [1.2.3](https://gitlab.qualiproto.fr/pyx4/i18n/compare/v1.2.2...v1.2.3) (2021-04-22)

## [1.2.2](https://gitlab.qualiproto.fr/pyx4/i18n/compare/v1.2.1...v1.2.2) (2021-04-22)

## [1.2.1](https://gitlab.qualiproto.fr/pyx4/i18n/compare/v1.2.0...v1.2.1) (2020-06-26)


### Bug Fixes

* Includes only production files in shipped files ([6255c89](https://gitlab.qualiproto.fr/pyx4/i18n/commit/6255c8987836e2b50f812570895f65ef5f6705b5))

# [1.2.0](https://gitlab.qualiproto.fr/pyx4/i18n/compare/v1.1.2...v1.2.0) (2020-06-17)


### Bug Fixes

* Adds meaningful error information to existing errors ([5307572](https://gitlab.qualiproto.fr/pyx4/i18n/commit/5307572eca6a3fc23ea6240860ff59647173a919))


### Features

* Exports new Error classes ([53006a1](https://gitlab.qualiproto.fr/pyx4/i18n/commit/53006a15f3264b75754ee596c981ec76600737bc))

## [1.1.2](https://gitlab.qualiproto.fr/pyx4/i18n/compare/v1.1.1...v1.1.2) (2020-06-16)


### Bug Fixes

* Export hooks from index ([8a255c6](https://gitlab.qualiproto.fr/pyx4/i18n/commit/8a255c6646549645d43e211db45e6bbf91f556a4))

## [1.1.1](https://gitlab.qualiproto.fr/pyx4/i18n/compare/v1.1.0...v1.1.1) (2020-06-16)


### Bug Fixes

* Add installation instructions to README ([7710a46](https://gitlab.qualiproto.fr/pyx4/i18n/commit/7710a46d663acb2799c68e2da874102f4daf043d))

# [1.1.0](https://gitlab.qualiproto.fr/pyx4/i18n/compare/v1.0.0...v1.1.0) (2020-06-16)


### Bug Fixes

* Validates createTranslator arguments ([a5ef825](https://gitlab.qualiproto.fr/pyx4/i18n/commit/a5ef8250d1d1273e0cb002c756f2c6bca0d891ff))


### Features

* Exposes the read-only default locale in configuration ([a56ae05](https://gitlab.qualiproto.fr/pyx4/i18n/commit/a56ae05690a9fb4776d97dd278707543e78bdaa0))
* Extends LocaleProvider exposing supported locales ([9c209ed](https://gitlab.qualiproto.fr/pyx4/i18n/commit/9c209ed2c5b6efdd3ac59f481a3e5ae94cfbdba8))
* Introduces the functional translator with React hooks ([6c2c753](https://gitlab.qualiproto.fr/pyx4/i18n/commit/6c2c7531593523280d86a66540d1ffe5d936131d))
* Introduces the locale configuration ([f6b333f](https://gitlab.qualiproto.fr/pyx4/i18n/commit/f6b333f4645a6b62434a0b220165c3cba9ca9e11))
* Introduces the translator ([cd83f21](https://gitlab.qualiproto.fr/pyx4/i18n/commit/cd83f215e747284a89605861d10b83f09f8beb26))

# 1.0.0 (2020-06-11)


### Features

* Introduces `assert` utility function ([357055a](https://gitlab.qualiproto.fr/pyx4/i18n/commit/357055af0c84addfdca40de12ef4bf501edd6eae))
