# I18n

This project is an internalization tool providing type-safe localized messages and date/time localization.  One key feature of this utility is that the use of messages that are not present in all supported languages will be flagged by errors by the Typescript compiler.

## Installation

Use your favorite package manager (`npm`, `pnpm`, `yarn`, etc.) to add this utility to your project.

```sh
$ yarn add @pyx4/i18n
```

## Usage

Re-export type-safe version of translation hook.  This provides type-safe auto-completions with your specific translation messages.

```tsx
import { useTranslator, TranslatorHook } from '@pxy4/i18n'
import { messages, supportedLocales } from './my/messages'

const typeSafeHook: TranslatorHook<typeof messages, typeof supportedLocales> =
  useTranslator

export { typeSafeHook as useTranslator }
```

Provide messages and locale configuration at the application level.

```tsx
import { Provider } from '@pyx4/i18n';
import { messages, supportedLocales } from './my/messages';

function Application() {
  return (
    <Provider
      defaultLocale="en"
      store={messages}
      supportedLocales={supportedLocales}>
      <Consumer />
    </Provider>
  );
}
```

Use translated messages where appropriate.  Below are a few examples with various degrees of complexity.

### Getting a single message

Find below an example of using this `useTranslator` hook to obtain a single translated message for the given key (`greeting`).

```tsx
import { useTranslator } from './my/re-export';

function Consumer() {
  const greeting = useTranslator(t => t('greeting'));

  return <>{greeting}</>;
}
```

### Getting a single parameterized message

In this example, the `named.greeting` message requires a `name` parameter for the message to be translated correctly.  Such parameters can be provided by supplying additional arguments to the `Translate` function passed to the `useTranslator` hook.

> As with other React hooks like `useCallback` or `useMemo`, `useTranslator` also accepts a dependency list to ensure that message caches are invalidated when a given dependency changes.

```tsx
import { useTranslator } from './my/re-export';

function Consumer() {
  const [name, setName] = useState('Maryam');
  const appendToName = useCallback(() => setName(`${name}a`));

  const greeting = useTranslator(
    t => t('named.greeting', name),
    [name]
  );

  return (
    <>
      <span>{greeting}</span>
      <button onClick={appendToName}>Add 'a' to name</button>
    </>
  );
}
```

### Getting multiple messages

In the following example, a separate selector function is defined to encapsulate the translation of several messages.  Whatever the selector returns will be returned by the `useTranslator` hook to which it is provided, with its messages translated within the current locale.

Using an external function like this declutters the function component, keeping it focused on what matters most.  It also allows the selector function to be tested independently if necessary or mocked since it's a plain JS function.

```tsx
import { useTranslator, Translate } from './my/re-export';

function selectMessages(t: Translate) {
  return {
    balanceTitle: t('account.balance.title'),
    title: t('app.title')
  };
}

function Consumer() {
  const { balanceTitle, title } = useTranslator(selectMessages);
  
  return (
    <>
      <h1>{title}</h1>
      <header>{balanceTitle}</header>
    </>
  );
}
```

### Getting multiple parameterized messages

Like the multi-message example above, this last example shows a combination of translating multiple messages and having some messages require parameters.  This uses a higher-order function (`messagesFor`) to return selector than can be passed to the `useTranslator` hook.

> If following this convention, the arguments passed to the higher-order function are _usually_ arguments you'll want to pass to the `useTranslator`'s dependency list, as seen below with `[name, balance]` for cache invalidation.

```tsx
import { useTranslator, Translate } from './my/re-export';

function messagesFor(name: string, balance: number) {
  return (t: Translate) => ({
    greeting: t('greeting.named', name),
    displayedBalances: t('account.balance', balance.toLocaleString(), balance)
  });
}

function Consumer() {
  const [name] = useState('Maryam')
  const balance = useAccountBalance('MY_ACCOUNT')
  const { displayedBalance, greeting } = useTranslator(
    messagesFor(name, balance),
    [name, balance]
  );

  return <>
    <span>{greeting}</span>
    <span>{displayedBalance}</span>
  </>
}
```

## Design goals

- Allow translation messages to be injected when the translation service is created
- Consume the translation service in component lifecycles instead of passing around global objects
- Provide a convention or practice for more neatly consuming/using translated messages (see example with multiple messages)
- Be extensible to support date/time localization

## The language of _Internationalization_

In the context of internationalization, the core domain models are the following:

| Domain model    | Purpose |
| ------------    | ------- |
| Locale configuration | provides a selected **locale**, supported locales and allows one to select a new locale |
| Translator      | accepts a unique **message key** (possibly with additional arguments) and returns a **message** 
| Localizer       | accepts a **date/time** and returns a textual form of that date/time appropriate for a given **locale** |

In addition to the above, some secondary domain models include:

| Secondary model | Purpose |
| --------------- | ------- |
| Message         | human-readable message translated for a given **locale** |
| Interpolator    | accepts a set of arguments and returns a **message** |
| Message store   | collection of **message**/**interpolator** maps keyed by **locale names** |
| Locale name     | a _more-or-less_ standard locale identifier (e.g.: `en`, `fr`) |

> A little context...  
> 
> The above may seem somewhat abstract and/or overly verbose but the main idea is to define a common language within the context of **internationalization** (I18n) so that what things are and what purpose they serve is either self-evident or clearly defined.

## How core models interact

Below is a rough dependency that demonstrates the relationship between the core models.

```mermaid
graph TD

store(Message store)

subgraph I18n Context
  locale_config(Locale configuration)
  translator(Translator)
  localizer(Localizer)
end

store --> locale_config
locale_config --> localizer
locale_config --> translator
```
