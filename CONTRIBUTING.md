# Setting up

Clone the repository

```sh
$ git clone ...
```

Install dependencies

```sh
$ yarn install
```

Test your code

```sh
$ yarn run test
```

# Contributing features, fixes, refactors, etc.

1. Create issues to track to features you intend to implement or bugs you intend to fix.  Remember to request feedback for designs before implementing them to ensure they align with the project's goals.
2. Create a `feature/*`, `fix/*` or appropriately named branch from `master` on which to commit your changes
3. Use [conventional commit][conventional-commit] messages when committing your work to your branch as this project respects semantic versioning.  This project automates releases so certain types of commits may increment the project's version.  See [examples][conventional-commit-examples] for more details.  Generally:
    - `fix` commits will increase the **patch** version
    - `feat` commits will increase the **minor** version
    - `BREAKING CHANGE` commits will incrase the **major** version
4. Submit a merge request to merge your branch into `master` including a summary of key changes.  When automated tests have passed, ping maintainers for code review.
5. Once the merge request has been accepted, maintainers will merge the branch into `master`.  It is your responsibility to clean up your remote branches.

> Note, maintainers will make a best-effort attempt to document in the merge request the first version in which the merged code is available.

[conventional-commit]: https://www.conventionalcommits.org/en/v1.0.0/
[conventional-commit-examples]: https://www.conventionalcommits.org/en/v1.0.0/#examples