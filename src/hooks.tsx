import * as React from 'react'
import {
  createContext,
  PropsWithChildren,
  Reducer,
  useContext,
  useMemo,
  useReducer
} from 'react'

import { MissingContext } from './errors'
import { createTranslator } from './translate'

import type {
  LocaleConfig,
  LocaleConfigHook,
  LocaleName,
  LocalesOf,
  MessageStore,
  Translate,
  TranslatorHook
} from './types'

const TranslationContext = createContext<Translate | null>(null)
const LocaleContext = createContext<LocaleConfig | null>(null)

type State<Locale extends LocaleName = LocaleName> = {
  locale: Locale
}

function initialize<Locale extends LocaleName>(locale: Locale): State<Locale> {
  return { locale }
}

function reduceLocale<Locale extends LocaleName>(
  state: State<Locale>,
  newLocale: Locale
): State<Locale> {
  if (state.locale === newLocale) return state

  return initialize(newLocale)
}

type Props<
  Messages extends MessageStore,
  Locales extends LocalesOf<Messages>
> = PropsWithChildren<{
  /**
   * Default locale to be used for localization and translation
   *
   * This is used when no `initialLocale` is provided or when the selected
   * locale does not offer the data requested (such as a missing translation).
   */
  defaultLocale: Locales[number]

  /**
   * Initial locale tobe selected
   */
  initialLocale?: Locales[number]

  /**
   * Store of locale-specific messages and message interpolators
   *
   * @see `MessageStore`
   */
  store: Messages

  /**
   * All supported locales in the message `store`
   */
  supportedLocales: LocaleConfig<Locales>['supportedLocales']
}>

/**
 * Provides the given locale configuration and message store for use with
 * locale-related hooks (e.g.: `useLocaleConfig`, `useTranslator`)
 */
export function Provider<
  Messages extends MessageStore,
  Locales extends LocalesOf<Messages>
>({
  children,
  defaultLocale,
  initialLocale,
  supportedLocales,
  store
}: Props<Messages, Locales>): JSX.Element {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const translate: Translate<Messages, any> = createTranslator(
    store,
    supportedLocales,
    defaultLocale
  )

  const [{ locale }, dispatch] = useReducer<
    Reducer<State<Locales[number]>, Locales[number]>,
    Locales[number]
  >(reduceLocale, initialLocale ?? defaultLocale, initialize)

  // NOTE: This _may_ be better separated into 2 contexts for performance but
  //   really only if many components use the `updateLocale`.
  const localeConfig: LocaleConfig<Locales> = {
    locale,
    setLocale: dispatch,
    supportedLocales
  }

  return (
    <LocaleContext.Provider value={localeConfig}>
      <TranslationContext.Provider value={translate}>
        {children}
      </TranslationContext.Provider>
    </LocaleContext.Provider>
  )
}

export const useLocaleConfig: LocaleConfigHook = () => {
  const config = useContext(LocaleContext)
  if (!config) throw new MissingContext('Locale')

  return config
}

export const useTranslator: TranslatorHook = (fn, deps) => {
  const dependencies = deps ?? []

  const config = useContext(LocaleContext)
  if (!config) throw new MissingContext('Locale')

  const translate = useContext(TranslationContext)
  if (!translate) throw new MissingContext('Translation')

  const translator = translate.withLocale(config.locale)

  return useMemo(() => fn(translator), [fn, translator, ...dependencies])
}
