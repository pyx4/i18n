import { MissingMessages, MissingSupportedLocale } from './errors'

import type {
  LocaleName,
  LocalesOf,
  Message,
  MessageKey,
  MessageStore,
  Translate,
  Translator
} from './types'

export function createTranslator<
  Messages extends MessageStore = MessageStore,
  Locales extends LocalesOf<Messages> = LocalesOf<Messages>
>(
  store: Messages,
  supportedLocales: Locales,
  defaultLocale: LocaleName = supportedLocales[0]
): Translate<Messages, Locales> {
  const availableLocales = Object.keys(store)
  supportedLocales.forEach((l) => {
    if (!availableLocales.includes(l))
      throw new MissingSupportedLocale(l, availableLocales)
  })

  function translate(
    locale: LocaleName,
    key: MessageKey,
    ...args: Array<unknown>
  ): Message {
    const messages = store[locale]
    if (!messages) throw new MissingMessages(locale)

    const message = messages[key]

    if (typeof message === 'string') return message

    // If the message is an interpolator, return its result with the given `args`
    if (typeof message === 'function') return message(...args)

    // If the message is neither of the approved expected types and requested
    // locale is NOT the default, attempt to use the default instead
    if (locale !== defaultLocale) return translate(defaultLocale, key, ...args)

    // We've attempt to use known types and the fallback, so return the key
    return key
  }

  const translatorCache: Partial<
    Record<Locales[number], Translator<Messages, Locales>>
  > = {}

  return Object.assign(translate, {
    withLocale(locale: Locales[number]): Translator<Messages, Locales> {
      const translator: Translator<Messages, Locales> =
        translatorCache[locale] ??
        ((key, ...args) => translate(locale, key, ...args))
      translatorCache[locale] = translator
      return translator
    }
  })
}
