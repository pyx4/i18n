import type { LocaleName } from './types'

export class MissingContext extends Error {
  constructor(contextName: string) {
    super(
      `${contextName} context was used before being provided.  Consult the ` +
        'i18n documentation for how to provide the required i18n context'
    )
  }
}

export class MissingMessages extends Error {
  constructor(locale: LocaleName) {
    super(`No messages for the request locale "${locale}"`)
  }
}

export class MissingSupportedLocale extends Error {
  constructor(locale: LocaleName, availableLocales?: Array<LocaleName>) {
    super(
      availableLocales
        ? `Locale "${locale}" cannot be supported as it is not available in ` +
            `the message store: [${availableLocales}].`
        : `Locale "${locale}" cannot be supported.`
    )
  }
}
