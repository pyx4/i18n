import { act, fireEvent, render, screen } from '@testing-library/react'
import { makeAutoObservable, reaction } from 'mobx'
import { useLocalObservable } from 'mobx-react-lite'
import * as React from 'react'
import { ReactNode, useCallback, useEffect, useState } from 'react'

import { Provider, useLocaleConfig, useTranslator } from '../src/hooks'

import type { TranslatorHook } from '../src/types'

const store = {
  en: {
    greeting: (name: string) => `Hello, ${name}!`,
    signoff: 'Goodbye'
  },
  fr: {
    greeting: (name: string) => `Bonjour, ${name}`,
    onlyFrench: 'Allo',
    signoff: 'Bonsoir'
  },
  ru: {
    onlyRussian: ''
  }
}

const supportedLocales = ['fr', 'en'] as const
type SupportedLocale = typeof supportedLocales[number]

function DefaultProvider({ children }: { children: ReactNode }) {
  const props = { store, supportedLocales, defaultLocale: 'en' } as const
  return <Provider {...props}>{children}</Provider>
}

describe('useTranslator', () => {
  const use: TranslatorHook<
    typeof store,
    typeof supportedLocales
  > = useTranslator

  function useMessages(name: string) {
    return use(
      (t) => ({
        header: t('greeting', name)
      }),
      [name]
    )
  }

  function Consumer({ name }: { name: string }) {
    const { header } = useMessages(name)

    return <>{header}</>
  }

  it('translates messages as expected', () => {
    const { container } = render(
      <DefaultProvider>
        <Consumer name="Grace" />
      </DefaultProvider>
    )

    expect(container.textContent).toContain(store.en.greeting('Grace'))
  })

  it('works', () => {
    const base = makeAutoObservable({
      locale: 'en',
      setLocale(newLocale: SupportedLocale) {
        this.locale = newLocale
      }
    })

    function LocaleSource() {
      const { setLocale } = useLocaleConfig()
      const user = useLocalObservable(() => ({
        get locale() {
          return base.locale
        },
        update(newLocale: SupportedLocale) {
          base.locale = newLocale
        }
      }))

      reaction(
        () => user.locale,
        (newLocale) => setLocale(newLocale)
      )

      return <></>
    }

    function Providers() {
      return (
        <Provider
          defaultLocale="en"
          store={store}
          supportedLocales={supportedLocales}
        >
          <Consumer name="Grace" />
          <LocaleSource />
        </Provider>
      )
    }

    const { container } = render(<Providers />)

    act(() => {
      base.setLocale('fr')
    })

    expect(container.textContent).toContain(store.fr.greeting('Grace'))
  })

  it('rerenders messages given changes in selected locale', () => {
    function LocaleChanger() {
      const { setLocale } = useLocaleConfig()

      useEffect(() => {
        setLocale('fr')
      }, [])

      return <></>
    }

    const { container } = render(
      <DefaultProvider>
        <Consumer name="Grace" />
        <LocaleChanger />
      </DefaultProvider>
    )

    expect(container.textContent).toContain(store.fr.greeting('Grace'))
  })

  it('rerenders messages given changes in message dependencies', () => {
    function Invoker() {
      const [name, setName] = useState('Grace')
      const updateName = useCallback(() => {
        setName('Jason')
      }, [setName])

      const { header } = useMessages(name)

      return (
        <>
          <button onClick={updateName} />
          {header}
        </>
      )
    }

    const { container } = render(
      <DefaultProvider>
        <Invoker />
      </DefaultProvider>
    )

    const button = screen.getByRole('button')

    act(() => {
      fireEvent.click(button)
    })

    expect(container.textContent).toContain(store.en.greeting('Jason'))
  })
})
