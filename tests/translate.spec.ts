import { MissingSupportedLocale } from '../src/errors'
import { createTranslator } from '../src/translate'

import type { Translate } from '../src/types'

const store = {
  en: {
    greeting: (name: string) => `Hello, ${name}!`,
    signoff: 'Goodbye'
  },
  fr: {
    greeting: (name: string) => `Bonjour, ${name}`,
    onlyFrench: 'Allo',
    signoff: 'Bonsoir'
  },
  ru: {
    onlyRussian: ''
  }
}

const supportedLocales = ['fr', 'en'] as const

describe(createTranslator, () => {
  it('throws a MissingSupportedLocale error if any supported locales are unavailable in the message store', () => {
    const createWithMissingLocales = () => {
      // @ts-expect-error
      createTranslator(store, ['es'] as const)
    }

    expect(createWithMissingLocales).toThrow(MissingSupportedLocale)
  })
})

describe('translate', () => {
  let translate: Translate<typeof store, typeof supportedLocales>
  const defaultLocale = 'fr'
  const selectedLocale = 'en'

  beforeEach(() => {
    translate = createTranslator(store, supportedLocales, defaultLocale)
  })

  describe('when the requested key matches a string message', () => {
    it('returns the string message', () => {
      const requestedKey = 'signoff'

      const result = translate(selectedLocale, requestedKey)

      expect(result).toEqual(store[selectedLocale][requestedKey])
    })
  })

  describe('when the requested key matches an interpolator', () => {
    it('calls the interpolator with any provided arguments and returns the message', () => {
      const requestedKey = 'greeting'

      const result = translate(selectedLocale, 'greeting', 'John')

      expect(result).toEqual(store[selectedLocale][requestedKey]('John'))
    })
  })

  describe('when the requested key does not match any entry in the selected locale', () => {
    it('returns a translation for the same key with the default locale', () => {
      const requestedKey = 'onlyFrench'

      // @ts-expect-error as only keys present in all supported locales are safe
      const result = translate(selectedLocale, requestedKey)

      expect(result).toEqual(store[defaultLocale].onlyFrench)
    })
  })

  describe('when the requested key does not match any entry in the default locale', () => {
    it('returns the requested key', () => {
      const requestedKey = 'asdf-invalid'

      // @ts-expect-error
      const result = translate(selectedLocale, requestedKey)

      expect(result).toEqual(requestedKey)
    })
  })

  describe('withLocale', () => {
    it('returns a translator bound to a specific locale', () => {
      const translator = translate.withLocale('fr')

      const result = translator('signoff')

      expect(result).toEqual(translate('fr', 'signoff'))
    })
  })
})
