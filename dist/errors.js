"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.MissingSupportedLocale = exports.MissingMessages = exports.MissingContext = void 0;
var MissingContext = /** @class */ (function (_super) {
    __extends(MissingContext, _super);
    function MissingContext(contextName) {
        return _super.call(this, contextName + " context was used before being provided.  Consult the " +
            'i18n documentation for how to provide the required i18n context') || this;
    }
    return MissingContext;
}(Error));
exports.MissingContext = MissingContext;
var MissingMessages = /** @class */ (function (_super) {
    __extends(MissingMessages, _super);
    function MissingMessages(locale) {
        return _super.call(this, "No messages for the request locale \"" + locale + "\"") || this;
    }
    return MissingMessages;
}(Error));
exports.MissingMessages = MissingMessages;
var MissingSupportedLocale = /** @class */ (function (_super) {
    __extends(MissingSupportedLocale, _super);
    function MissingSupportedLocale(locale, availableLocales) {
        return _super.call(this, availableLocales
            ? "Locale \"" + locale + "\" cannot be supported as it is not available in " +
                ("the message store: [" + availableLocales + "].")
            : "Locale \"" + locale + "\" cannot be supported.") || this;
    }
    return MissingSupportedLocale;
}(Error));
exports.MissingSupportedLocale = MissingSupportedLocale;
