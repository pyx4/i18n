import type { LocaleName } from './types';
export declare class MissingContext extends Error {
    constructor(contextName: string);
}
export declare class MissingMessages extends Error {
    constructor(locale: LocaleName);
}
export declare class MissingSupportedLocale extends Error {
    constructor(locale: LocaleName, availableLocales?: Array<LocaleName>);
}
