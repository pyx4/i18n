/**
 * A human-readable messaged translated for a given **locale**
 *
 * @alias string
 */
export declare type Message = string;
/**
 * A key that uniquely refers to a single **message**
 *
 * @alias string
 */
export declare type MessageKey = string;
/**
 * A function accepting a set of arguments and returning a **message**
 *
 * @returns a **message**
 * @example
 *
 * const greeting: Interpolator<[string]> = (name) => `Hello, ${name}!`
 */
export declare type Interpolator<Args extends Array<unknown> = Array<any>> = (...args: Args) => Message;
/**
 * A conventional locale identifier
 *
 * @alias string
 * @example
 *
 * const defaultLocale: LocaleName = 'en'
 * const locale: LocaleName = 'fr'
 */
export declare type LocaleName = string;
/**
 * A collection of **message** / **interpolator** maps keyed by **locale names**
 *
 * @example
 *
 * const store: MessageStore = {
 *   en: {
 *     greeting: 'Hello!',
 *     namedGreeting: (name: string) => `Hello, ${name}!`
 *   },
 *   fr: {
 *     greeting: 'Bonjour!',
 *     namedGreeting: (name: string) => `Bonjour, ${name}!`
 *   }
 * }
 */
export declare type MessageStore = Record<LocaleName, Record<string, Message | Interpolator>>;
/**
 * Allows one to see the current locale, see supported locales and select a new
 * locale
 */
export interface LocaleConfig<Locales extends ReadonlyArray<LocaleName> = ReadonlyArray<LocaleName>> {
    /**
     * Currently selected locale
     */
    readonly locale: Locales[number];
    /**
     * All supported locales
     */
    readonly supportedLocales: Locales;
    /**
     * Updates the current locale to be the `requestedLocale`
     *
     * @param requestedLocale to be used
     */
    setLocale(requestedLocale: Locales[number]): void;
}
/**
 * Returns the locale configuration made available by the provider.
 *
 * @see `Provider` to make a configurable available to this hook
 * @see `LocaleConfig`
 */
export interface LocaleConfigHook<Locales extends ReadonlyArray<LocaleName> = ReadonlyArray<LocaleName>> {
    (): LocaleConfig<Locales>;
}
/**
 * Returns a translated message from the message store matching the provided
 * `key` under the given `locale`.  If the given `key` matches a message
 * `Interpolator`, this function may accept or require additional arguments to
 * return a properly interpolated message in the requested `locale`.
 *
 * @see `MessageStore` for an example of a valid message store
 * @throws an error if the given `locale` does not exist in the message store
 * @example
 *
 * const translate = createTranslator(store, ['en', 'fr'] as const)
 *
 * // Getting a generic greeting
 * const message = translate('en', 'greeting')
 * > 'Hello!'
 *
 * // Getting a named greeting (requiring interpolation)
 * const message = translate('fr', 'namedGreeting', 'Elizabeth')
 * > 'Bonjour, Elizabeth!'
 */
export interface Translate<Messages extends MessageStore = MessageStore, Locales extends LocalesOf<Messages> = LocalesOf<Messages>> {
    <Key extends KeyOf<Messages, Locales>>(locale: Locales[number], key: Key, ...args: ArgsOf<Messages, Locales, Key>): Message;
    withLocale(locale: Locales[number]): Translator<Messages, Locales>;
}
/**
 * Returns a translated message from the message store matching the provided
 * `key` under the predetermined locale.
 *
 * @note This is a partial invocation of `Translate` with the `locale` already
 *   provided
 * @see `Translate` for behavioral details
 * @example
 *
 * const t = createTranslator(store, ['en', 'fr'] as const).withLocale('en')
 *
 * // Getting a generic greeting
 * const message = t('greeting')
 * > 'Hello!'
 *
 * // Greeting a named greeting (requiring interpolation)
 * const message = t('namedGreeting', 'Grace')
 * > 'Hello, Grace!'
 */
export declare type Translator<Messages extends MessageStore = MessageStore, Locales extends LocalesOf<Messages> = LocalesOf<Messages>> = <Key extends KeyOf<Messages, Locales>>(key: Key, ...args: ArgsOf<Messages, Locales, Key>) => Message;
/**
 * Invokes the provided `fn` callback with `Translator` specific to the current
 * locale and returns its result.
 *
 * @param fn callback accepting a locale-specific `Translator`
 * @param deps optional array of dependencies that if different, will require
 *   re-invoking the provided callback
 * @see `Translator` for an example of what the callback can expect to receive
 *   as its first argument
 * @see `LocaleConfigHook` for determining with the current locale is
 *
 * @example
 *
 * function Component() {
 *   const [name] = useState('Grace')
 *   const message = useTranslator(
 *     t => t('namedGreeting', name),
 *     [name]
 *   )
 *
 *   return <>{message}</>
 * }
 */
export declare type TranslatorHook<Messages extends MessageStore = MessageStore, Locales extends LocalesOf<Messages> = LocalesOf<Messages>> = <Result>(fn: TranslatorCallback<Result, Messages, Locales>, deps?: ReadonlyArray<unknown>) => Result;
/**
 * Callback required by `useTranslator` that is given a locale-specific
 * `Translator` function
 *
 * @internal
 */
declare type TranslatorCallback<Result, Messages extends MessageStore = MessageStore, Locales extends LocalesOf<Messages> = LocalesOf<Messages>> = (translator: Translator<Messages, Locales>) => Result;
/**
 * Read-only array type of locales from a given `Messages` message store type
 *
 * @internal
 */
export declare type LocalesOf<Messages extends MessageStore> = ReadonlyArray<keyof Messages & LocaleName>;
/**
 * Type of all **safe** message keys in the given `Messages` message store type
 *
 * Only keys present in all `Locales` of the given `Messages` message store are
 * considered **safe**.
 *
 * @internal
 */
declare type KeyOf<Messages extends MessageStore, Locales extends LocalesOf<Messages>> = keyof Messages[Locales[number]] & MessageKey;
/**
 * Type of arguments supported by interpolators matching a given message `Key`.
 * If the given `Key` matches a `Message` and not an `Interpolator`, there are
 * no supported arguments (represented by an empty tuple `[]`).
 *
 * @internal
 */
declare type ArgsOf<Messages extends MessageStore, Locales extends LocalesOf<Messages>, Key extends KeyOf<Messages, Locales>> = Messages[Locales[number]][Key] extends Interpolator ? Parameters<Messages[Locales[number]][Key]> : [];
export {};
