import type { LocaleName, LocalesOf, MessageStore, Translate } from './types';
export declare function createTranslator<Messages extends MessageStore = MessageStore, Locales extends LocalesOf<Messages> = LocalesOf<Messages>>(store: Messages, supportedLocales: Locales, defaultLocale?: LocaleName): Translate<Messages, Locales>;
