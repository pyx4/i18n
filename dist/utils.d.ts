/**
 * Throws an `Error` if the provided `condition` is not met.
 *
 * @param condition
 * @param msg included the error if the condition is not met
 */
export declare function assert(condition: unknown, msg: string): asserts condition;
