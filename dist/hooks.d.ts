import { PropsWithChildren } from 'react';
import type { LocaleConfig, LocaleConfigHook, LocalesOf, MessageStore, TranslatorHook } from './types';
declare type Props<Messages extends MessageStore, Locales extends LocalesOf<Messages>> = PropsWithChildren<{
    /**
     * Default locale to be used for localization and translation
     *
     * This is used when no `initialLocale` is provided or when the selected
     * locale does not offer the data requested (such as a missing translation).
     */
    defaultLocale: Locales[number];
    /**
     * Initial locale tobe selected
     */
    initialLocale?: Locales[number];
    /**
     * Store of locale-specific messages and message interpolators
     *
     * @see `MessageStore`
     */
    store: Messages;
    /**
     * All supported locales in the message `store`
     */
    supportedLocales: LocaleConfig<Locales>['supportedLocales'];
}>;
/**
 * Provides the given locale configuration and message store for use with
 * locale-related hooks (e.g.: `useLocaleConfig`, `useTranslator`)
 */
export declare function Provider<Messages extends MessageStore, Locales extends LocalesOf<Messages>>({ children, defaultLocale, initialLocale, supportedLocales, store }: Props<Messages, Locales>): JSX.Element;
export declare const useLocaleConfig: LocaleConfigHook;
export declare const useTranslator: TranslatorHook;
export {};
