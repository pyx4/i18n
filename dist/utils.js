"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.assert = void 0;
/**
 * Throws an `Error` if the provided `condition` is not met.
 *
 * @param condition
 * @param msg included the error if the condition is not met
 */
function assert(condition, msg) {
    if (!condition)
        throw new Error(msg);
}
exports.assert = assert;
