"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTranslator = void 0;
var errors_1 = require("./errors");
function createTranslator(store, supportedLocales, defaultLocale) {
    if (defaultLocale === void 0) { defaultLocale = supportedLocales[0]; }
    var availableLocales = Object.keys(store);
    supportedLocales.forEach(function (l) {
        if (!availableLocales.includes(l))
            throw new errors_1.MissingSupportedLocale(l, availableLocales);
    });
    function translate(locale, key) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var messages = store[locale];
        if (!messages)
            throw new errors_1.MissingMessages(locale);
        var message = messages[key];
        if (typeof message === 'string')
            return message;
        // If the message is an interpolator, return its result with the given `args`
        if (typeof message === 'function')
            return message.apply(void 0, args);
        // If the message is neither of the approved expected types and requested
        // locale is NOT the default, attempt to use the default instead
        if (locale !== defaultLocale)
            return translate.apply(void 0, __spreadArray([defaultLocale, key], args));
        // We've attempt to use known types and the fallback, so return the key
        return key;
    }
    var translatorCache = {};
    return Object.assign(translate, {
        withLocale: function (locale) {
            var _a;
            var translator = (_a = translatorCache[locale]) !== null && _a !== void 0 ? _a : (function (key) {
                var args = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    args[_i - 1] = arguments[_i];
                }
                return translate.apply(void 0, __spreadArray([locale, key], args));
            });
            translatorCache[locale] = translator;
            return translator;
        }
    });
}
exports.createTranslator = createTranslator;
