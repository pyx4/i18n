"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useTranslator = exports.useLocaleConfig = exports.Provider = void 0;
var React = __importStar(require("react"));
var react_1 = require("react");
var errors_1 = require("./errors");
var translate_1 = require("./translate");
var TranslationContext = react_1.createContext(null);
var LocaleContext = react_1.createContext(null);
function initialize(locale) {
    return { locale: locale };
}
function reduceLocale(state, newLocale) {
    if (state.locale === newLocale)
        return state;
    return initialize(newLocale);
}
/**
 * Provides the given locale configuration and message store for use with
 * locale-related hooks (e.g.: `useLocaleConfig`, `useTranslator`)
 */
function Provider(_a) {
    var children = _a.children, defaultLocale = _a.defaultLocale, initialLocale = _a.initialLocale, supportedLocales = _a.supportedLocales, store = _a.store;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    var translate = translate_1.createTranslator(store, supportedLocales, defaultLocale);
    var _b = react_1.useReducer(reduceLocale, initialLocale !== null && initialLocale !== void 0 ? initialLocale : defaultLocale, initialize), locale = _b[0].locale, dispatch = _b[1];
    // NOTE: This _may_ be better separated into 2 contexts for performance but
    //   really only if many components use the `updateLocale`.
    var localeConfig = {
        locale: locale,
        setLocale: dispatch,
        supportedLocales: supportedLocales
    };
    return (React.createElement(LocaleContext.Provider, { value: localeConfig },
        React.createElement(TranslationContext.Provider, { value: translate }, children)));
}
exports.Provider = Provider;
var useLocaleConfig = function () {
    var config = react_1.useContext(LocaleContext);
    if (!config)
        throw new errors_1.MissingContext('Locale');
    return config;
};
exports.useLocaleConfig = useLocaleConfig;
var useTranslator = function (fn, deps) {
    var dependencies = deps !== null && deps !== void 0 ? deps : [];
    var config = react_1.useContext(LocaleContext);
    if (!config)
        throw new errors_1.MissingContext('Locale');
    var translate = react_1.useContext(TranslationContext);
    if (!translate)
        throw new errors_1.MissingContext('Translation');
    var translator = translate.withLocale(config.locale);
    return react_1.useMemo(function () { return fn(translator); }, __spreadArray([fn, translator], dependencies));
};
exports.useTranslator = useTranslator;
